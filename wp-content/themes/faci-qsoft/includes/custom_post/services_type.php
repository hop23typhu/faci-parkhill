<?php
/** Services **/
$args = array(
    "label"                         => "Services Categories", 
    "singular_label"                => "Services Category", 
    'public'                        => true,
    'hierarchical'                  => true,
    'show_ui'                       => true,
    'show_in_nav_menus'             => false,
    'args'                          => array( 'orderby' => 'term_order' ),
    'rewrite'                       => false,
    'query_var'                     => true
);
register_taxonomy( 'service-category', 'service', $args );
    


add_action('init', 'services_register');  
function services_register() {  
    global $themename;
    $labels = array(
        'name'               => __('Services', 'post type general name', $themename),
        'singular_name'      => __('Service', 'post type singular name', $themename),

    );

    $args = array(  
        'labels'            => $labels,  
        'public'            => true,  
        'show_ui'           => true,
        'show_in_menu'      => true,
        'show_in_nav_menus' => false,
        'rewrite'           => false,
        'supports'          => array('title', 'editor', 'thumbnail'),
        'has_archive'       => true,
        'menu_icon'         => 'dashicons-awards',
        'taxonomies'        => array('service-category'),
       );  
  
    register_post_type( 'service' , $args );  
}

?>