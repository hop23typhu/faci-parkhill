<!DOCTYPE html>
<html class="no-js" lang="en-US">
<head itemscope itemtype="http://schema.org/WebSite">
<base href="<?php bloginfo( 'wpurl' ); ?>/">
	<meta charset="UTF-8">
	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<!--[if IE ]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<![endif]-->
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	


    <link rel="pingback" href="http://parkhilltimescity.com/xmlrpc.php" />

<script type="text/javascript">document.documentElement.className = document.documentElement.className.replace( /\bno-js\b/,'js' );</script>
<!-- All in One SEO Pack 2.3.2.3 by Michael Torbert of Semper Fi Web Design[1045,1072] -->
<meta name="description" itemprop="description" content="Times City Park Hill thuộc giai đoạn 2 KĐT Times City. Tọa lạc gồm căn hộ đẳng cấp – Shop house, siêu thị... tạo nên mô hình sống nghỉ dưỡng đầu tiên tại Hà Nội" />

<meta name="keywords" itemprop="keywords" content="Times City Park Hill, Vinhomes Times City Park Hill, Park Hill Times City, chung cư Times City Park Hill" />

<link rel="canonical" href="http://parkhilltimescity.com/" />
<!-- /all in one seo pack -->
<link rel="alternate" type="application/rss+xml" title=" &raquo; Feed" href="http://parkhilltimescity.com/feed/" />
<link rel="alternate" type="application/rss+xml" title=" &raquo; Comments Feed" href="http://parkhilltimescity.com/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/parkhilltimescity.com\/wp-includes\/js\/wp-emoji-release.min.js"}};
			!function(a,b,c){function d(a){var c,d=b.createElement("canvas"),e=d.getContext&&d.getContext("2d"),f=String.fromCharCode;return e&&e.fillText?(e.textBaseline="top",e.font="600 32px Arial","flag"===a?(e.fillText(f(55356,56806,55356,56826),0,0),d.toDataURL().length>3e3):"diversity"===a?(e.fillText(f(55356,57221),0,0),c=e.getImageData(16,16,1,1).data.toString(),e.fillText(f(55356,57221,55356,57343),0,0),c!==e.getImageData(16,16,1,1).data.toString()):("simple"===a?e.fillText(f(55357,56835),0,0):e.fillText(f(55356,57135),0,0),0!==e.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag"),unicode8:d("unicode8"),diversity:d("diversity")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag&&c.supports.unicode8&&c.supports.diversity||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='fontawesome-css'  href="<?php echo get_stylesheet_directory_uri(); ?>/plugins/wp-mega-menu/css/font-awesome.min.css"  type='text/css' media='all' />
<link rel='stylesheet' id='wpmm-css'  href="<?php echo get_stylesheet_directory_uri(); ?>/plugins/wp-mega-menu/css/wpmm.css"  type='text/css' media='all' />
<link rel='stylesheet' id='tablepress-default-css'  href="<?php echo get_stylesheet_directory_uri(); ?>/plugins/tablepress/css/default.min.css"  type='text/css' media='all' />
<link rel='stylesheet' id='tipsy-css'  href="<?php echo get_stylesheet_directory_uri(); ?>/plugins/wp-shortcode/css/tipsy.css"  type='text/css' media='all' />
<link rel='stylesheet' id='mts_wpshortcodes-css'  href="<?php echo get_stylesheet_directory_uri(); ?>/plugins/wp-shortcode/css/wp-shortcode.css"  type='text/css' media='all' />
<link rel='stylesheet' id='best-stylesheet-css'  href="wp-content/themes/faci-qsoft/style.css"  type='text/css' media='all' />
<style id='best-stylesheet-inline-css' type='text/css'>

        body {background-color:#ffffff;background-image:url(wp-content/themes/faci-qsoft/images/nobg.png);}
        .main-header {background-color:#ffffff;background-image:url(wp-content/themes/faci-qsoft/images/nobg.png);}
        footer-carousel-wrap {background-color:#23705b; }
        footer {background-color:#eeeeee;background-image:url(wp-content/themes/faci-qsoft/images/nobg.png);}
        footer > .copyrights {background-color:#FFFFFF;}
        .pace .pace-progress, .mobile-menu-wrapper, .owl-carousel .owl-nav > div, #top-navigation li:hover a, #header nav#top-navigation ul ul li, a#pull, .secondary-navigation, #move-to-top,.mts-subscribe input[type='submit'],input[type='submit'],#commentform input#submit,.contactform #submit,.pagination a,.fs-pagination a,.header-search .ajax-search-results-container,#load-posts a,#fs2_load_more_button,#wp-calendar td a,#wp-calendar caption,#wp-calendar #prev a:before,#wp-calendar #next a:before, .tagcloud a, #tags-tab-content a, #wp-calendar thead th.today, .slide-title, .slidertitle, #header nav#navigation ul ul li, .thecategory a, #wp-calendar td a:hover, #wp-calendar #today, .widget .wpt-pagination a, .widget .wpt_widget_content #tags-tab-content ul li a, .widget .wp_review_tab_widget_content .wp-review-tab-pagination a, .ajax-search-meta .results-link, .post-day .review-total-only, .woocommerce a.button, .woocommerce-page a.button, .woocommerce button.button, .woocommerce-page button.button, .woocommerce input.button, .woocommerce-page input.button, .woocommerce #respond input#submit, .woocommerce-page #respond input#submit, .woocommerce #content input.button, .woocommerce-page #content input.button, .woocommerce nav.woocommerce-pagination ul li a, .woocommerce-page nav.woocommerce-pagination ul li a, .woocommerce #content nav.woocommerce-pagination ul li a, .woocommerce-page #content nav.woocommerce-pagination ul li a, .woocommerce .bypostauthor:after, #searchsubmit, .woocommerce nav.woocommerce-pagination ul li a:hover, .woocommerce-page nav.woocommerce-pagination ul li a:hover, .woocommerce #content nav.woocommerce-pagination ul li a:hover, .woocommerce-page #content nav.woocommerce-pagination ul li a:hover, .woocommerce nav.woocommerce-pagination ul li a:focus, .woocommerce-page nav.woocommerce-pagination ul li a:focus, .woocommerce #content nav.woocommerce-pagination ul li a:focus, .woocommerce-page #content nav.woocommerce-pagination ul li a:focus, .woocommerce a.button, .woocommerce-page a.button, .woocommerce button.button, .woocommerce-page button.button, .woocommerce input.button, .woocommerce-page input.button, .woocommerce #respond input#submit, .woocommerce-page #respond input#submit, .woocommerce #content input.button, .woocommerce-page #content input.button, .widget_product_search input[type='submit'] {background: #157057; color: #fff; }
        .header-search #s,nav a.toggle-mobile-menu, .tab_widget ul.wps_tabs li, .wpt_widget_content .tab_title.selected a, .widget_wp_review_tab .tab_title.selected a {background: #157057 !important;}
        #wp-calendar thead th.today { border-color: #157057; }
        a, a:hover,.title a:hover,.post-data .post-title a:hover,.post-title a:hover,.post-info a:hover,.entry-content a,.textwidget a,.reply a,.comm,.fn a,.comment-reply-link, .entry-content .singleleft a:hover, #footer-post-carousel .owl-nav div {color:#157057;}
        .post-box .review-total-only .review-result-wrapper .review-result i {color:#157057!important;}
        footer > .footer-carousel-wrap { background: #23705b; }
        
        .shareit { top: 373px; left: auto; z-index: 0; margin: 0 0 0 -110px; width: 100px; position: fixed; padding: 0; border:none; border-right: 0;}
        .share-item {margin: 2px;}
        
        
        
        
        .main-header {background: url(wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/bg_header.jpg) center top no-repeat #1a505a;}
.ad-728{
	-webkit-box-shadow: 0px 0px 8px 0px rgba(0, 0, 0, 0.3);
	-moz-box-shadow:    0px 0px 8px 0px rgba(0, 0, 0, 0.3);
	box-shadow:         0px 0px 8px 0px rgba(0, 0, 0, 0.3);	
}
.box_support{ width:100%;}
.box_support h3{width:298px; font-family:Roboto !important; font-size:20px; text-align:center; background:#23705b; line-height:36px; margin-bottom:1px; color:#fff;}
.box_support div.content_support{ background:url(wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/bg_support.jpg) no-repeat; width:100%; height:146px; margin-bottom:20px;}
.box_support div.content_support p{ font-family:Roboto !important; font-size:28px; text-align:center; color:#fff; font-weight:bold; line-height:9px !important;}
.box_support div.content_support p:nth-child(1){ color:#faff1a; padding-top:40px;}

#menu-mat-bang{ margin:0px !important;}
#menu-mat-bang li{
	background:url(wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/box_design1.jpg) no-repeat;
	width:299x; 
	height:46px;
	line-height:46px;
	font-family:Roboto !important;
	font-size:16px;
	margin-bottom:10px;
}
#menu-mat-bang li:nth-child(2){background:url(wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/box_design2.jpg) no-repeat;}
#menu-mat-bang li:nth-child(3){background:url(wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/box_design3.jpg) no-repeat;}
#menu-mat-bang li:nth-child(4){background:url(wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/box_design4.jpg) no-repeat;}
#menu-mat-bang li:last-child{ margin-bottom:0px;}
#menu-mat-bang li a{ color:#fff !important; text-indent:35px; display:inline-block;}
#menu-mat-bang li a:hover{ color:#6F0 !important; background:none !important;}

.last{ width:26.5% !important;}
.ad-125 li{max-width: 47% !important;}
.ad-125 li img{ width:140px !important;}
.sidebar_list .widget h3 {
  background: url("wp-content/themes/faci-qsoft/uploads/sites/133/2015/11/bg_title.jpg") no-repeat scroll 0 0;
  color: #fff;
  font-size: 18px !important;
  line-height: 50px !important;
  text-align: center;
  padding-left: 0px;
}
#menu-park-hill-premium li, #menu-park-hill-premium li a{color: #282828; font-family: "Roboto"; font-size: 16px; font-weight: normal;line-height: 40px;}
#menu-park-hill-premium li:nth-child(2n+1){ background: url(wp-content/themes/faci-qsoft/uploads/sites/133/2015/11/bullet1.jpg) #ebebeb no-repeat 10px 9px; }
#menu-park-hill-premium li:nth-child(2n+2){background: url(wp-content/themes/faci-qsoft/uploads/sites/133/2015/11/bullet2.jpg) no-repeat 10px 9px;}
#menu-park-hill-premium li a:hover{ background:#157057;}
#menu-park-hill-premium li a{ padding-left:46px;}
#menu-park-hill-premium li a:hover{ background:none; color:#157057;}
#menu-park-hill-premium{margin-left:0 !important}


            
</style>
<link rel='stylesheet' id='owl-carousel-css'  href="wp-content/themes/faci-qsoft/css/owl.carousel.css"  type='text/css' media='all' />
<link rel='stylesheet' id='magnificPopup-css'  href="wp-content/themes/faci-qsoft/css/magnific-popup.css"  type='text/css' media='all' />
<link rel='stylesheet' id='responsive-css'  href="wp-content/themes/faci-qsoft/css/responsive.css"  type='text/css' media='all' />
<script type='text/javascript' src="wp-includes/js/jquery/jquery.js" ></script>
<script type='text/javascript' src="wp-includes/js/jquery/jquery-migrate.min.js" ></script>
<script type='text/javascript'>
/* <![CDATA[ */
var mts_customscript = {"responsive":"1","nav_menu":"secondary"};
/* ]]> */
</script>
<script type='text/javascript' async="async" src="wp-content/themes/faci-qsoft/js/customscript.js" ></script>
<script type='text/javascript' src="../html5shim.googlecode.com/svn/trunk/html5.js" ></script>
<script type='text/javascript' src="<?php echo get_stylesheet_directory_uri(); ?>/plugins/optin-monster/assets/js/api.js-t=1461316815&ver=2.1.7.js" ></script>
<script type='text/javascript' src="<?php echo get_stylesheet_directory_uri(); ?>/plugins/wp-shortcode/js/jquery.tipsy.js" ></script>
<script type='text/javascript' src="<?php echo get_stylesheet_directory_uri(); ?>/plugins/wp-shortcode/js/wp-shortcode.js" ></script>
<link rel='https://api.w.org/' href='http://parkhilltimescity.com/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://parkhilltimescity.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://parkhilltimescity.com/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.4.2" />
<link rel='shortlink' href='http://parkhilltimescity.com/' />
<script src="../web4.ancu.com/-dm=830c492b36869f99911b5221fc4516a2&action=load&blogid=133&siteid=1&t=233598412&back=http---parkhilltimescity.com-.js"  type='text/javascript'></script><link href="../fonts.googleapis.com/css-family=Roboto-700-Roboto-normal&subset=latin.css"  rel="stylesheet" type="text/css">
<style type="text/css">
#logo a { font-family: 'Roboto'; font-weight: 700; font-size: 36px; color: #0e227f;text-transform: uppercase; }
.menu li, .menu li a { font-family: 'Roboto'; font-weight: normal; font-size: 18px; color: #141414; }
body { font-family: 'Roboto'; font-weight: normal; font-size: 15px; color: #444444; }
.post-data .post-title a, #comments-tab-content a { font-family: 'Roboto'; font-weight: 700; font-size: 16px; color: #444444;text-transform: uppercase; }
.hentry .entry-title { font-family: 'Roboto'; font-weight: 700; font-size: 18px; color: #444444;text-transform: uppercase; }
#sidebars .widget { font-family: 'Roboto'; font-weight: normal; font-size: 15px; color: #444444; }
.footer-widgets { font-family: 'Roboto'; font-weight: normal; font-size: 16px; color: #444444; }
h1 { font-family: 'Roboto'; font-weight: 700; font-size: 16px; color: #444444;text-transform: uppercase; }
h2 { font-family: 'Roboto'; font-weight: 700; font-size: 14px; color: #444444;text-transform: uppercase; }
h3 { font-family: 'Roboto'; font-weight: 700; font-size: 20px; color: #444444;text-transform: uppercase; }
h4 { font-family: 'Roboto'; font-weight: 700; font-size: 18px; color: #444444;text-transform: uppercase; }
h5 { font-family: 'Roboto'; font-weight: 700; font-size: 15px; color: #444444;text-transform: uppercase; }
h6 { font-family: 'Roboto'; font-weight: 700; font-size: 13px; color: #444444;text-transform: uppercase; }
</style>
</head>
<body id ="blog" class="home page page-id-727 page-template-default main" itemscope itemtype="http://schema.org/WebPage">
	<div class="main-container-wrap">
		<header id="site-header" role="banner" class="main-header" itemscope itemtype="http://schema.org/WPHeader">
			<div id="header">
				<div class="container">
					<div class="header-inner">
						<div class="logo-wrap">
																								<h1 id="logo" class="image-logo" itemprop="headline">
										<a href="index.htm" ><img src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/10/logo_newstarland.png"  alt=""></a>
									</h1><!-- END #logo -->
																					</div>
						<div id="mts_ad_728_widget-2" class="widget-header"><div class="ad-728"><a href="park-hill-premium/index.htm" ><img src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/10/parkhill_premium_728x90.gif"  width="728" height="90" alt="" /></a></div></div>					</div>
				</div><!--.container-->
				
															<div class="clear" id="catcher"></div>
						<div class="secondary-navigation sticky-navigation" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
											<div class="container clearfix">
							<a href="#" id="pull" class="toggle-mobile-menu">Menu</a>
															<nav id="navigation" class="clearfix mobile-menu-wrapper">
																			<ul id="menu-main-menu" class="menu clearfix"><li  id="menu-item-734" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-727 current_page_item menu-item-734"><a title="" target="" rel="" href="index.htm"  style=""><i class="fa fa-home wpmm-menu-icon"></i> GIỚI THIỆU</a><style type="text/css">
</style></li>
<li  id="menu-item-794" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-794"><a title="" target="" rel="" href="park-hill-premium/index.htm"  style=""><i class="fa fa-leaf wpmm-menu-icon"></i> PARK HILL PREMIUM</a><style type="text/css">
</style>
<ul class="sub-menu">
	<li  id="menu-item-1440" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1440"><a title="" target="" rel="" href="shophouse-park-hill-times-city-dau-tu-an-toan-sinh-loi-cao/index.htm"  style="">SHOPHOUSE PARK HILL</a><style type="text/css">
</style></li>
</ul>
</li>
<li  id="menu-item-1276" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1276"><a title="" target="" rel="" href="thong-tin-moi-nhat-ve-du-an-vinhomes-gardenia/index.htm"  style="">KĐT GARDENIA</a><style type="text/css">
</style>
<ul class="sub-menu">
	<li  id="menu-item-1307" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1307"><a title="" target="" rel="" href="chung-cu-vinhomes-arcadia/index.htm"  style="">VINHOMES ARCADIA</a><style type="text/css">
</style></li>
	<li  id="menu-item-1314" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1314"><a title="" target="" rel="" href="biet-thu-vinhomes-botanica/index.htm"  style="">VINHOMES BOTANICA</a><style type="text/css">
</style></li>
</ul>
</li>
<li  id="menu-item-27" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-27"><a title="" target="" rel="" href="category/thong-tin-mo-ban/index.htm"  style=""><i class="fa fa-bell wpmm-menu-icon"></i> THÔNG TIN MỞ BÁN</a><style type="text/css">
</style>
<ul class="sub-menu">
	<li  id="menu-item-951" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-951"><a title="" target="" rel="" href="toa-park-1-times-city/index.htm"  style=""><i class="fa fa-star wpmm-menu-icon"></i> MỞ BÁN TÒA PARK 1</a><style type="text/css">
</style></li>
	<li  id="menu-item-705" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-705"><a title="" target="" rel="" href="park-2-times-city-khong-gian-xanh-cho-cuoc-song-mat-lanh-ben-vung/index.htm"  style=""><i class="fa fa-star wpmm-menu-icon"></i> MỞ BÁN TÒA PARK 2</a><style type="text/css">
</style></li>
	<li  id="menu-item-617" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-617"><a title="" target="" rel="" href="mo-ban-chung-cu-park-5-vinhomes-times-city/index.htm"  style=""><i class="fa fa-star wpmm-menu-icon"></i> MỞ BÁN TÒA PARK 5</a><style type="text/css">
</style></li>
	<li  id="menu-item-468" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-468"><a title="" target="" rel="" href="ban-chung-cu-park-6-park-hill-times-city-vinhomes/index.htm"  style=""><i class="fa fa-star wpmm-menu-icon"></i> MỞ BÁN TÒA PARK 6</a><style type="text/css">
</style></li>
	<li  id="menu-item-467" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-467"><a title="" target="" rel="" href="ban-chung-cu-park-3-vinhomes-times-city/index.htm"  style=""><i class="fa fa-star wpmm-menu-icon"></i> MỞ BÁN TÒA PARK 3</a><style type="text/css">
</style></li>
	<li  id="menu-item-823" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-823"><a title="" target="" rel="" href="park-7-park-8-times-city/index.htm"  style=""><i class="fa fa-star wpmm-menu-icon"></i> TÒA PARK 7 &amp; PARK 8</a><style type="text/css">
</style></li>
</ul>
</li>
<li  id="menu-item-18" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-18"><a title="" target="" rel="" href="category/thiet-ke-can-ho/index.htm"  style=""><i class="fa fa-building wpmm-menu-icon"></i> THIẾT KẾ</a><style type="text/css">
</style>
<ul class="sub-menu">
	<li  id="menu-item-1010" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1010"><a title="" target="" rel="" href="mat-bang-park-1-times-city/index.htm"  style="">MẶT BẰNG CĂN HỘ PARK 1</a><style type="text/css">
</style></li>
	<li  id="menu-item-897" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-897"><a title="" target="" rel="" href="mat-bang-can-ho-park-7-park-hill-times-city/index.htm"  style="">THIẾT KẾ CĂN HỘ PARK 7</a><style type="text/css">
</style></li>
	<li  id="menu-item-896" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-896"><a title="" target="" rel="" href="mat-bang-can-ho-park-8-park-hill-times-city/index.htm"  style="">THIẾT KẾ CĂN HỘ PARK 8</a><style type="text/css">
</style></li>
	<li  id="menu-item-715" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-715"><a title="" target="" rel="" href="thiet-ke-mat-bang-chung-cu-park-2-times-city/index.htm"  style="">THIẾT KẾ CĂN HỘ PARK 2</a><style type="text/css">
</style></li>
	<li  id="menu-item-626" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-626"><a title="" target="" rel="" href="thiet-ke-mat-bang-can-1-phong-ngu-park-5-vinhomes-times-city/index.htm"  style="">THIẾT KẾ CĂN HỘ PARK 5</a><style type="text/css">
</style></li>
	<li  id="menu-item-471" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-471"><a title="" target="" rel="" href="thiet-ke-mat-bang-toa-park-6/index.htm"  style="">MẶT BẰNG TÒA PARK 6</a><style type="text/css">
</style></li>
	<li  id="menu-item-472" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-472"><a title="" target="" rel="" href="ban-chung-cu-park-6-park-hill-times-city-vinhomes/index.htm"  style="">THIẾT KẾ CĂN HỘ PARK 6</a><style type="text/css">
</style></li>
	<li  id="menu-item-21" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-21"><a title="" target="" rel="" href="thiet-ke-toa-park-3-vinhomes-times-city-park-hill/index.htm"  style="">MẶT BẰNG TÒA PARK 3</a><style type="text/css">
</style></li>
	<li  id="menu-item-22" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-22"><a title="" target="" rel="" href="mat-bang-thiet-ke-can-ho-park-3-times-city/index.htm"  style="">THIẾT KẾ CĂN HỘ PARK 3</a><style type="text/css">
</style></li>
</ul>
</li>
<li  id="menu-item-20" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-20"><a title="" target="" rel="" href="lien-he-mua-ban-chung-cu-vinhomes-times-city-park-hill/index.htm"  style="">LIÊN HỆ</a><style type="text/css">
</style></li>
</ul>																	</nav>
													</div>
					</div>
				
			</div><!--#header-->
		</header>
		<div class="main-container">		<div id="page" class="single">
			<article class="article">
		<div id="content_box" >
							<div id="post-727" class="g post post-727 page type-page status-publish has-post-thumbnail has_thumb">
					<div class="single_page">
												<header>
							<h1 class="title entry-title">Times City Park Hill &#8211; Sống ưu việt- chuẩn tương lai!</h1>
						</header>
						<div class="post-content box mark-links entry-content">
														
							<p style="text-align: justify">Tiếp nối thành công của giai đoạn 1 khu đô thị Times City, chủ đầu tư Vingroup chính thức cho ra mắt giai đoạn 2: <strong><a href="index.htm" >Park Hill Times City</a></strong> với ý tưởng về một khu đô thị sinh thái đẳng cấp, mang đậm phong cách Singapore cùng tiêu chí về một phong cách &#8220;sống resort trong lòng đô thị&#8221;. Tất cả sản phẩm của giai đoạn 2 này, ngay từ khi bắt đầu ra mắt đã &#8220;làm mưa làm gió&#8221;trên thị trường bất động sản Hà Nội cũng như toàn Việt Nam ở phân khúc chung cư cao cấp.</p>
<h3 style="text-align: center"><span style="color: #ff0000">HOT!!! CHÍNH SÁCH BÁN HÀNG TÒA PARK 9 + 11+ 12- THÁNG 4/2016</span><strong><a href="wp-content/themes/faci-qsoft/uploads/sites/133/2015/06/icon-6.png" ><br />
</a></strong></h3>
<p><strong> </strong><strong>MIỄN PHÍ GÓI DỊCH VỤ <span style="color: #ff0000">05 NĂM</span> cho khách hàng mua căn hộ Park 11 Park Hill Premium</strong><strong><a href="wp-content/themes/faci-qsoft/uploads/sites/133/2015/06/icon-6.png" ><img class="alignleft wp-image-1358" src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/06/icon-6.png"  alt="icon-6" width="20" height="20" srcset="http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/06/icon-6-65x65.png 65w, http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/06/icon-6.png 90w" sizes="(max-width: 20px) 100vw, 20px" /></a>Chiết khấu lên tới</strong><span style="color: #ff0000"><strong> 7%/năm</strong></span></p>
<p><strong><a href="wp-content/themes/faci-qsoft/uploads/sites/133/2015/06/icon-6.png" ><img class="alignleft wp-image-1358" src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/06/icon-6.png"  alt="icon-6" width="20" height="20" srcset="http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/06/icon-6-65x65.png 65w, http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/06/icon-6.png 90w" sizes="(max-width: 20px) 100vw, 20px" /></a></strong>Khách hàng thanh toán trước hạn sẽ được hưởng mức chiết khấu tương đương 7%/năm trên khoản tiền và số ngày thanh toán trước hạn. Khoản tiền sẽ được chiết khấu thẳng vào giá bán căn hộ.</p>
<p><strong><a href="wp-content/themes/faci-qsoft/uploads/sites/133/2015/06/icon-6.png" ><img class="alignleft wp-image-1358" src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/06/icon-6.png"  alt="icon-6" width="20" height="20" srcset="http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/06/icon-6-65x65.png 65w, http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/06/icon-6.png 90w" sizes="(max-width: 20px) 100vw, 20px" /></a>Quà tặng hấp dẫn: </strong>Tặng Vé vui chơi giải trí tại TTTM Vincom Mega Mall Times City và Royal City. Số lượng 10 vé/năm trong vòng 03 năm với tổng giá trị <strong><span style="color: #ff0000">6.000.000đồng</span>/căn.</strong></p>
<p><strong><a href="wp-content/themes/faci-qsoft/uploads/sites/133/2015/06/icon-6.png" ><img class="alignleft wp-image-1358" src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/06/icon-6.png"  alt="icon-6" width="20" height="20" srcset="http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/06/icon-6-65x65.png 65w, http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/06/icon-6.png 90w" sizes="(max-width: 20px) 100vw, 20px" /></a>Hỗ trợ vay lên tới</strong><strong> 85 %</strong> giá trị căn hộ với lãi suất ưu đãi</p>
<p><strong><a href="wp-content/themes/faci-qsoft/uploads/sites/133/2015/06/icon-6.png" ><img class="alignleft wp-image-1358" src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/06/icon-6.png"  alt="icon-6" width="20" height="20" srcset="http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/06/icon-6-65x65.png 65w, http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/06/icon-6.png 90w" sizes="(max-width: 20px) 100vw, 20px" /></a>CÁC GÓI SẢN PHẨM / DỊCH VỤ CÓ THỂ LỰA CHỌN:</strong></p>
<p>+ Gói Dịch vụ quản lý căn hộ 10 năm trả trước<br />
<span style="line-height: 1.5">+ Gói 10 năm nước nóng<br />
</span><span style="line-height: 1.5">+ Gói Dịch vụ Vinmec<br />
</span><span style="line-height: 1.5">+ Gói Dịch vụ Vinschool<br />
</span><span style="line-height: 1.5">+ Gói Dịch vụ nghỉ dưỡng Vinpearl</span></p>
<p><div class="button-center"><a href="#register-form" target="_self" class="buttons btn_red center"><span class="left">SỐ LƯỢNG CÓ HẠN- NHẬN QUÀ TẶNG NGAY HÔM NAY </span></a></div><strong><a href="wp-content/themes/faci-qsoft/uploads/sites/133/2015/06/icon-6.png" ><br />
</a></strong></p>
<p><strong><img class="alignleft wp-image-1382 size-full" src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/06/icon_new_en.gif"  alt="icon_new_en" width="30" height="20" /><a href="park-12-park-hill-premium/index.htm" >Cập nhật bảng giá và ưu đãi Tòa PARK 12</a></strong></p>
<p><a href="wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/Park-hill-times-city-phoi-canh-ngoai-troi-3.jpg" ><img class="wp-image-84 aligncenter" src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/Park-hill-times-city-phoi-canh-ngoai-troi-3.jpg"  alt="Park-hill-times-city-phoi-canh-ngoai-troi-3" width="800" height="371" srcset="http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/Park-hill-times-city-phoi-canh-ngoai-troi-3-300x139.jpg 300w, http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/Park-hill-times-city-phoi-canh-ngoai-troi-3.jpg 804w" sizes="(max-width: 800px) 100vw, 800px" /></a></p>
<p style="text-align: center">Phổi cảnh Vinhomes Times City Park Hill về đêm</p>
<p style="text-align: center"><div class="button-center"><a href="#register-form" target="_self" class="buttons btn_red center"><span class="left">ĐĂNG KÝ MUA CĂN HỘ PARK 9 VÀ PARK 11 </span></a></div></p>
<h2>TỔNG QUAN DỰ ÁN PARK HILL</h2>
<p style="text-align: center"><strong><span style="color: #ff0000">****<a style="color: #ff0000" href="360/index.htm" >THAM QUAN TỔNG THỂ &amp; NHÀ MẪU PARK HILL TIMES CITY TRỰC TUYẾN</a>****</span></strong></p>
<p style="text-align: justify">Khu đô thị Vinhomes Times City Park Hill được quy hoạch với 8 tòa căn hộ từ Park 1 đến Park 8. Trong đó, tính đến thời điểm hiện tại, đã có <a href="ban-chung-cu-park-3-vinhomes-times-city/index.htm" >Park 3 Times City</a>, Park 6 Park Hill, Park 5 Times City và mới đây nhất là <a href="park-2-times-city-khong-gian-xanh-cho-cuoc-song-mat-lanh-ben-vung/index.htm" >Park 2 Times City</a> được chủ đầu tư Vingroup chính thức mở bán.</p>
<ul style="text-align: justify">
<li>Tổng diện tích đất: 74.204m2</li>
<li>Diện tích đất xây dựng: 16.411,9m2</li>
<li>Tỷ lệ diện tích dành cho cây xanh và không gian cảnh quan: trên 70%</li>
</ul>
<p style="text-align: justify">Theo đó, toàn bộ Park Hill sẽ được bao phủ bởi không gian xanh với tâm điểm là đồi Vọng Cảnh . Từ đây, khoảng xanh cây cỏ sẽ được nối liền liên tục giữa bể bơi trung tâm với không gian khoáng đạt của quảng trường Times Square, xen kẽ với những khu vường và tiếp nối với màu xanh của những &#8220;bức tường&#8221; cây bao quanh.</p>
<p style="text-align: right"><em><a href="park-hill-premium/index.htm" >Đăng ký tư vấn Park Hill Premium ngay!!!!</a></em></p>
<h2 style="text-align: justify">Vị trí đắc địa</h2>
<p style="text-align: justify">Nói đến vị trí đắc địa của Times City Park Hill không thể không nhắc tới vị trí cửa ngõ Đông Nam thủ đô Hà Nội- một địa điểm vô cùng thuận lợi trong kết nối những quận nội thành cũng như các tỉnh lân cận phía Nam:</p>
<div id="attachment_80" style="width: 814px" class="wp-caption aligncenter"><a href="wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/Park-hill-times-city-lien-ket-khu-vuc.jpg" ><img class="size-full wp-image-80" src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/Park-hill-times-city-lien-ket-khu-vuc.jpg"  alt="Park-hill-times-city-lien-ket-khu-vuc" width="804" height="618" srcset="http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/Park-hill-times-city-lien-ket-khu-vuc-300x230.jpg 300w, http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/Park-hill-times-city-lien-ket-khu-vuc.jpg 804w" sizes="(max-width: 804px) 100vw, 804px" /></a><p class="wp-caption-text">Vị trí cửa ngõ của Park Hill Times City</p></div>
<p style="text-align: justify">Từ vị trí này,</p>
<ul style="text-align: justify">
<li>Times City Park Hill chỉ cách trung tâm Hà Nội 4km.</li>
<li>Ngay cạnh chân cầu Vĩnh Tuy, dễ dàng kết nối với các tỉnh phía Nam thông qua quốc lộ 5 hay hệ thống biệt thự Vinhomes Riverside.</li>
<li>Kết nối với cửa ngõ phía Tây vào trung tâm thành phố bằng hệ thống đường cao tốc trên cao hiện đại cùng hệ thống đường vành đai 2 mở rộng đoạn ngã 4 sở- cầu Vĩnh Tuy.</li>
</ul>
<h2 style="text-align: justify">Thiết kế căn hộ thông minh vượt trội</h2>
<p style="text-align: justify">Tất cả những tòa căn hộ đã được chủ đầu tư Vingroup đưa ra thị trường đều được cư dân và giới chuyên gia đánh giá cao về những ưu điểm vượt trội trong thiết kế, vừa đem thiên nhiên vào từng phòng trong căn hộ, vừa đảm bảo tính sang trọng, đẳng cấp như mục tiêu vốn có của Vingroup.</p>
<div id="attachment_85" style="width: 781px" class="wp-caption aligncenter"><a href="wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/Park-hill-times-city-phoi-canh-noi-that-phong-khach.jpg" ><img class="size-full wp-image-85" src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/Park-hill-times-city-phoi-canh-noi-that-phong-khach.jpg"  alt="Park-hill-times-city-phoi-canh-noi-that-phong-khach" width="771" height="551" srcset="http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/Park-hill-times-city-phoi-canh-noi-that-phong-khach-300x214.jpg 300w, http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/Park-hill-times-city-phoi-canh-noi-that-phong-khach.jpg 771w" sizes="(max-width: 771px) 100vw, 771px" /></a><p class="wp-caption-text">Thiết kế thông minh trong từng căn hộ tại Park Hill Times City</p></div>
<p style="text-align: justify"><span style="color: #ff0000"><em><a style="color: #ff0000" href="category/thiet-ke-can-ho/index.htm" >Xem thêm thiết kế căn hộ tại Park Hill Times City</a></em></span></p>
<ul style="text-align: justify">
<li>Thiết kế căn hộ xẻ khe thông minh, tận dụng được ánh sáng tự nhiên đến từng phòng trong căn hộ, vừa giúp tránh được tình trạng tự kỉ của nhà phố lại tiết kiệm năng lượng.</li>
<li>Tất cả cột chịu lực trong từng căn hộ được thay thế hoàn toàn bằng tường chịu lực, vừa tăng diện tích sử dụng lại giúp căn hộ trở nên vuông vắn và dễ dàng trong thiết kế nội thất.</li>
<li>Đặc biệt, các kiến trúc sư sẽ sử dụng kính xanh cao cấp- chiếm đến 70% diện tích mặt của tòa nhà để đem thiên nhiên vào từng không gian sống và mở rộng tầm nhìn xuống khoảng xanh bao la của toàn bộ quần thể.</li>
</ul>
<h2 style="text-align: justify">Hệ thống dịch vụ tiện ích sang trọng, đẳng cấp</h2>
<p style="text-align: justify">Nhắc đến Park Hill Times City, ngoài ưu điểm vượt trội trong thiết kế căn hộ, người ta còn nghĩ ngay đến hệ thống dịch vụ tiện ích đa dạng, đáp ứng nhu cầu của tất cả thế hệ nơi đây. Tất cả không chỉ phục vụ tiện nghi sống và nhu cầu vui chơi giải trí mà còn hướng đến thỏa mãn nhu cầu cao cấp trong chăm sóc sức khỏe, tinh thần của người dân.</p>
<p style="text-align: justify">Cư dân Park Hill sẽ được tận hưởng một cuộc sống &#8220;thực sự Singapore&#8221; với dịch vụ và tiện ích hoàn hảo, với không gian xanh dạo mát cho người cao tuổi, khu vực quảng trường và sân chơi phun nước quy mô lớn đầu tiên tại Hà Nội cho trẻ em; khu phố mua sắm sầm uất và khu cafe ngoài trời với wifi miễn phí dành cho cư dân năng động.</p>
<div id="attachment_96" style="width: 814px" class="wp-caption aligncenter"><a href="wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/Park-hill-Times-City-phong-cach-song-nghi-duong.png" ><img class="size-full wp-image-96" src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/Park-hill-Times-City-phong-cach-song-nghi-duong.png"  alt="Tiện ích tại Park Hill Times City" width="804" height="467" srcset="http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/Park-hill-Times-City-phong-cach-song-nghi-duong-300x174.png 300w, http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/Park-hill-Times-City-phong-cach-song-nghi-duong.png 804w" sizes="(max-width: 804px) 100vw, 804px" /></a><p class="wp-caption-text">Tiện ích vượt trội tại Vinhomes Times City Park Hill</p></div>
<p style="text-align: justify">Những cư dân yêu thể thao cũng sẽ được thỏa mãn niềm đam mê tối đa với nhiều lựa chọn: bóng rổ, bể bơi trong nhà và ngoài trời,&#8230; Tất cả đều được đầu tư và xây dựng theo tiêu chuẩn quốc tế.</p>
<p style="text-align: justify">Một tiện ích nổi trội, chỉ có ở Vinhomes Times City Park Hill nữa chính là vườn BBQ dành cho những bữa tiệc nướng ngoài trời. Cùng với đồi Vọng Cảnh, vườn BBQ sẽ tạo điều kiện tổ chức những buổi dã ngoại về với thiên nhiên ngay trong lòng khu đô thị.</p>
<p style="text-align: justify">Một cuộc sống xanh trọn vẹn, vừa tận hưởng thiên nhiên, vừa không xa rời tiện nghi sống năng động và hiện đại chính là món quà mà chủ đầu tư dành tặng cho khách hàng thông qua việc lựa chọn mô hình &#8220;sống resort trong lòng đô thị&#8221; cho Vinhomes Times City. Đây cũng là xu hướng mới, đang được ưa chuộng tại các khu đô thị hiện đại trên thế giới.</p>
<p style="text-align: justify">Quý khách quan tâm, có thể <strong>Trải nghiệm không gian sống Resort Park Hill</strong> ngay từ máy tính của mình, tại <span style="color: #ff0000"><a style="color: #ff0000" href="360/index.htm" >THĂM QUAN PARK HILL ONLINE</a></span></p>
<p style="text-align: justify">Hiện tại tòa căn hộ: Park 2 Times City, <a id="register-form" href="mo-ban-chung-cu-park-5-vinhomes-times-city/index.htm" >Park 5 Park Hill</a> đang được mở bán với chính sách ưu đãi khủng. Qúy khách có nhu cầu tìm hiểu thêm thông tin hay đăng ký tham quan nhà mẫu, vui lòng liên hệ:</p>
<p style="text-align: justify"><!-- This site converts visitors into subscribers and customers with the OptinMonster WordPress plugin v2.1.7 - http://optinmonster.com/ -->
<div id="om-hze5slkgvl-post" class="optin-monster-overlay" style=""><script type="text/javascript" src="../ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" ></script><style type="text/css" class="om-theme-sample-styles">.optin-monster-success-message {font-size: 21px;font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;color: #282828;font-weight: 300;text-align: center;margin: 0 auto;}.optin-monster-success-overlay .om-success-close {font-size: 32px !important;font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif !important;color: #282828 !important;font-weight: 300 !important;position: absolute !important;top: 0px !important;right: 10px !important;background: none !important;text-decoration: none !important;width: auto !important;height: auto !important;display: block !important;line-height: 32px !important;padding: 0 !important;}.om-helper-field {display: none !important;visibility: hidden !important;opacity: 0 !important;height: 0 !important;line-height: 0 !important;}html div#om-hze5slkgvl-post * {box-sizing:border-box;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;}html div#om-hze5slkgvl-post {background:none;border:0;border-radius:0;-webkit-border-radius:0;-moz-border-radius:0;float:none;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;height:auto;letter-spacing:normal;outline:none;position:static;text-decoration:none;text-indent:0;text-shadow:none;text-transform:none;width:auto;visibility:visible;overflow:visible;margin:0;padding:0;line-height:1;box-sizing:border-box;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-shadow:none;-moz-box-shadow:none;-ms-box-shadow:none;-o-box-shadow:none;box-shadow:none;-webkit-appearance:none;}html div#om-hze5slkgvl-post .om-clearfix {clear: both;}html div#om-hze5slkgvl-post .om-clearfix:after {clear: both;content: ".";display: block;height: 0;line-height: 0;overflow: auto;visibility: hidden;zoom: 1;}html div#om-hze5slkgvl-post #om-post-sample-optin {background: #fff;position: relative;padding: 20px;text-align: center;margin: 0 auto;max-width: 100%;width: 100%;}html div#om-hze5slkgvl-post #om-post-sample-optin-title {font-size: 18px;color: #222;width: 100%;margin-bottom: 15px;}html div#om-hze5slkgvl-post #om-post-sample-optin-tagline {font-size: 16px;line-height: 1.25;color: #484848;width: 100%;margin-bottom: 15px;}html div#om-hze5slkgvl-post input,html div#om-hze5slkgvl-post #om-post-sample-optin-name,html div#om-hze5slkgvl-post #om-post-sample-optin-email {background-color: #fff;width: 100%;border: 1px solid #ddd;font-size: 16px;line-height: 24px;padding: 4px 6px;overflow: hidden;outline: none;margin: 0 0 10px;vertical-align: middle;display: inline;color: #222;height: 34px;}html div#om-hze5slkgvl-post input[type=submit],html div#om-hze5slkgvl-post button,html div#om-hze5slkgvl-post #om-post-sample-optin-submit {background: #ff370f;border: 1px solid #ff370f;color: #fff;font-size: 16px;padding: 4px 6px;line-height: 24px;text-align: center;vertical-align: middle;cursor: pointer;display: inline;margin: 0;width: 100%;}html div#om-hze5slkgvl-post input[type=checkbox],html div#om-hze5slkgvl-post input[type=radio] {-webkit-appearance: checkbox;width: auto;outline: invert none medium;padding: 0;margin: 0;}</style><div id="om-post-sample-optin" class="om-post-sample om-clearfix om-theme-sample om-custom-html-form" style="background-color:#ffffff"><div id="om-post-sample-optin-wrap" class="om-clearfix"><div id="om-post-sample-header" class="om-clearfix" data-om-action="selectable"><div id="om-post-sample-optin-title" data-om-action="editable" data-om-field="title" style="color:#222222;font-family:Helvetica;font-size:18px;"><span style="font-size:20px;"><span style="color:#008080;"><span style="font-weight:bold;"><span style="font-family:roboto;">CAM KẾT TƯ VẤN CH&Iacute;NH X&Aacute;C, NHIỆT T&Igrave;NH, TẬN T&Acirc;M</span></span></span></span></div></div><div id="om-post-sample-content" class="om-clearfix" data-om-action="selectable"><div id="om-post-sample-optin-tagline" data-om-action="editable" data-om-field="tagline" style="color:#484848;font-family:Helvetica;font-size:16px;"><span style="font-size:24px;"><span style="font-weight:bold;"><span style="color:#FF0000;">HOTLINE: 0965 85 56 85</span></span></span></div></div><div id="om-post-sample-footer" class="om-clearfix om-has-email" data-om-action="selectable"><form name="Vinhomes Parkhill" action="http://crm.ancu.com/modules/Webforms/capture.php" method="post" accept-charset="utf-8" enctype="multipart/form-data">
   <input type="hidden" name="__vtrftk" value="sid:dd1da5e57c1aa3afa3c8175cfe670d604076147b,1457338947">
   <input type="hidden" name="publicid" value="d1e29e9c487e85f090f3b197d2ba3b7a">
   <input type="hidden" name="name" value="Vinhomes Parkhill">
   <input type="hidden" name="VTIGER_RECAPTCHA_PUBLIC_KEY" value="RECAPTCHA PUBLIC KEY FOR THIS DOMAIN">
   <table style="width:100%;border:none;">
      <tbody>
         <tr>
            <td>
               <input type="text" placeholder="Họ và Tên*" name="lastname" value="" required="">                
            </td>
         </tr>
         <tr>
            <td>
               <input type="text" placeholder="Số điện thoại*" name="mobile" value="" required="">                
            </td>
         </tr>
         <tr>
            <td>
               <input type="email" placeholder="Email" name="email" value="">                
            </td>
         </tr>
         <tr style="display:none">
            <td><input name="changeDesc" value="Park Hill - Nhu cầu Park Hill Premium" hidden=""></td>
         </tr>
         <tr style="display:none">
            <td>
               <select name="leadsource" hidden="">
                  <option value="">Select Value</option>
                  <option value="Website" selected="">Website</option>
               </select>
            </td>
         </tr>
         <tr style="display:none">
            <td>
               <input type="hidden" placeholder="Campaign" name="label:Campaign" value="">                
            </td>
         </tr>
      </tbody>
   </table>
   <input type="submit" value="ĐĂNG KÝ">
</form></div></div><input type="email" name="email" value="" class="om-helper-field" /><input type="text" name="website" value="" class="om-helper-field" /></div><script type="text/javascript">jQuery(document).ready(function($){WebFont.load({google: {families: ['Roboto']}});});</script></div>
<script type="text/javascript">var hze5slkgvl_post, omo = {"id":256,"optin":"hze5slkgvl-post","campaign":"in post","clones":[""],"hash":"hze5slkgvl-post","optin_js":"hze5slkgvl_post","type":"post","theme":"sample","cookie":0,"delay":0,"second":false,"exit":false,"redirect":"http:\/\/parkhilltimescity.com\/thank-you\/","redirect_pass":false,"custom":true,"test":false,"global_cookie":false,"preview":false,"ajax":"http:\/\/parkhilltimescity.com\/?optin-monster-ajax-route=1","mobile":false,"post_id":727,"preloader":"http:\/\/parkhilltimescity.com\/wp-content\/plugins\/optin-monster\/assets\/css\/images\/preloader.gif","error":"There was an error with your submission. Please try again.","ajax_error":"There was an error with the AJAX request: ","name_error":"Please enter a valid name.","email_error":"Please enter a valid email address.","bot_error":"Honeypot fields have been activated. Your submission is being flagged as potential spam.","success":"Thanks for subscribing! Please check your email for further instructions."}; hze5slkgvl_post = new OptinMonster(); hze5slkgvl_post.init(omo);</script>
<!--[if lte IE 9]><script type="text/javascript">var om_ie_browser = true;</script><![endif]--><!-- / OptinMonster WordPress plugin. -->
</p>
							
							
						</div><!--.post-content box mark-links-->
					</div>
				</div>
				<!-- You can start editing here. -->
<!-- If comments are closed. -->
<p class="nocomments"></p>

					</div>
	</article>
	<aside id="sidebar" class="sidebar c-4-12" role="complementary" itemscope itemtype="http://schema.org/WPSideBar">
	<div id="text-2" class="widget widget_text">			<div class="textwidget"><div class="box_support">
<h3>TƯ VẤN DỰ ÁN</h3>
<div class="content_support">
<p>18006646</p>
<p>0965 85 56 85</p>
<div>
</div>
</div>
		</div><div id="mts_ad_300_widget-4" class="widget mts_ad_300_widget"><h3 class="widget-title">PARK HILL PREMIUM</h3><div class="ad-300"><a href="park-hill-premium/index.htm" ><img src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/10/parkhill_premium_300x250_theme.gif"  width="300" height="250" alt="" /></a></div></div><div id="nav_menu-3" class="widget widget_nav_menu"><h3 class="widget-title">PARK HILL PREMIUM</h3><div class="menu-park-hill-premium-container"><ul id="menu-park-hill-premium" class="menu"><li  id="menu-item-1232" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1232"><a title="" target="" rel="" href="park-hill-premium/index.htm"  style="">PARK HILL PREMIUM</a><style type="text/css">
</style></li>
<li  id="menu-item-1233" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1233"><a title="" target="" rel="" href="toa-park-9-park-hill-premium/index.htm"  style="">PARK 9 PARK HILL PREMIUM</a><style type="text/css">
</style></li>
<li  id="menu-item-1235" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1235"><a title="" target="" rel="" href="toa-park-10-park-hill-premium/index.htm"  style="">PARK 10 PARK HILL PREMIUM</a><style type="text/css">
</style></li>
<li  id="menu-item-1236" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1236"><a title="" target="" rel="" href="park-11-park-hill-times-city/index.htm"  style="">PARK 11 PARK HILL PREMIUM</a><style type="text/css">
</style></li>
<li  id="menu-item-1237" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1237"><a title="" target="" rel="" href="park-12-park-hill-premium/index.htm"  style="">PARK 12 PARK HILL PREMIUM</a><style type="text/css">
</style></li>
</ul></div></div><div id="mts_ad_300_widget-5" class="widget mts_ad_300_widget"><h3 class="widget-title">NHÀ MẪU PARK HILL TRỰC TUYẾN</h3><div class="ad-300"><a href="360/index.htm" ><img src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/11/banner_parkhill_300x250.gif"  width="300" height="250" alt="" /></a></div></div><div id="mts_ad_300_widget-3" class="widget mts_ad_300_widget"><h3 class="widget-title">MỞ BÁN TÒA PARK 1</h3><div class="ad-300"><a href="toa-park-1-times-city/index.htm" ><img src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/09/parkhill_300x2501.jpg"  width="300" height="250" alt="" /></a></div></div><div id="mts_ad_widget-6" class="widget mts_ad_widget"><h3 class="widget-title">CÁC DỰ ÁN VINHOMES</h3><div class="ad-125"><ul><li class="oddad"><a href="#"><img src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/04/banner140x140_2.jpg"  width="125" height="125" alt="" /></a></li><li class="evenad"><a href="#"><img src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/banner_140x140_1.jpg"  width="125" height="125" alt="" /></a></li><li class="oddad"><a href="#"><img src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/banner_140x140_4.jpg"  width="125" height="125" alt="" /></a></li><li class="evenad"><a href="#"><img src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/banner140x140_3.jpg"  width="125" height="125" alt="" /></a></li></ul></div></div><div id="facebook-like-widget-2" class="widget facebook_like"><h3 class="widget-title">FACEBOOK PARK HILL</h3>			<div class="fb-page" data-href="javascript:if(confirm(%27https://www.facebook.com/ParkHill.TimesCity.Vinhomes  \n\nThis file was not retrieved by Teleport Pro, because it is addressed using an unsupported protocol (e.g., gopher).  \n\nDo you want to open it from the server?%27))window.location=%27https://www.facebook.com/ParkHill.TimesCity.Vinhomes%27"  data-width="292" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"></div>
		</div></aside><!--#sidebar-->
        </div><!--#page-->
    </div><!--.main-container-->
    <footer id="site-footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
                <div class="footer-carousel-wrap">
            <div class="container">
                <div class="slider-container">
                    <div class="owl-container loading">
                        <div id="footer-post-carousel" class="slides">
                                                        <div class="footer-carousel-item show-post-data">
                                <div class="dark-style post-box">
                                    <div class="post-data">
                                        <header>
                                            <a href="biet-thu-vinhomes-botanica/index.htm"  class="title post-title" title="Biệt thự Vinhomes Botanica">Biệt thự Vinhomes Botanica</a>
                                                <div class="post-info">  
                                                    <span class="thetime updated">December 20, 2015</span>    
                                                </div>
                                        </header>
                                    </div>
                                </div>
                                <a href="biet-thu-vinhomes-botanica/index.htm" >
                                    <img width="115" height="61" src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/09/park-1-park-hill.png"  class="attachment-best-widgetthumb size-best-widgetthumb wp-post-image" alt="Chung cư Vinhomes Arcadia" title="" srcset="http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/09/park-1-park-hill-300x159.png 300w, http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/09/park-1-park-hill-1024x542.png 1024w, http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/09/park-1-park-hill.png 1155w" sizes="(max-width: 115px) 100vw, 115px" />                                </a>
                            </div>
                                                        <div class="footer-carousel-item">
                                <div class="dark-style post-box">
                                    <div class="post-data">
                                        <header>
                                            <a href="chung-cu-vinhomes-arcadia/index.htm"  class="title post-title" title="Chung cư Vinhomes Arcadia">Chung cư Vinhomes Arcadia</a>
                                                <div class="post-info">  
                                                    <span class="thetime updated">December 20, 2015</span>    
                                                </div>
                                        </header>
                                    </div>
                                </div>
                                <a href="chung-cu-vinhomes-arcadia/index.htm" >
                                    <img width="115" height="65" src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/12/chung-cu-vinhomes-arcadia.png"  class="attachment-best-widgetthumb size-best-widgetthumb wp-post-image" alt="chung-cu-vinhomes-arcadia" title="" />                                </a>
                            </div>
                                                        <div class="footer-carousel-item">
                                <div class="dark-style post-box">
                                    <div class="post-data">
                                        <header>
                                            <a href="thong-tin-moi-nhat-ve-du-an-vinhomes-gardenia/index.htm"  class="title post-title" title="Thông tin mới nhất về dự án Vinhomes Gardenia">Thông tin mới nhất về dự án Vinhomes Gardenia</a>
                                                <div class="post-info">  
                                                    <span class="thetime updated">December 20, 2015</span>    
                                                </div>
                                        </header>
                                    </div>
                                </div>
                                <a href="thong-tin-moi-nhat-ve-du-an-vinhomes-gardenia/index.htm" >
                                    <img width="115" height="69" src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/12/vi-tri-vinhomes-gardenia-cau-dien.jpg"  class="attachment-best-widgetthumb size-best-widgetthumb wp-post-image" alt="Vinhomes Gardenia" title="" srcset="http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/12/vi-tri-vinhomes-gardenia-cau-dien-300x180.jpg 300w, http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/12/vi-tri-vinhomes-gardenia-cau-dien.jpg 853w" sizes="(max-width: 115px) 100vw, 115px" />                                </a>
                            </div>
                                                        <div class="footer-carousel-item">
                                <div class="dark-style post-box">
                                    <div class="post-data">
                                        <header>
                                            <a href="chinh-sach-ban-hang-uu-dai-times-city-park-hill-thang-122015/index.htm"  class="title post-title" title="Chính sách bán hàng &amp; ưu đãi Times City Park Hill Tháng 12/2015">Chính sách bán hàng &amp; ưu đãi Times City Park Hill Tháng 12/2015</a>
                                                <div class="post-info">  
                                                    <span class="thetime updated">December 14, 2015</span>    
                                                </div>
                                        </header>
                                    </div>
                                </div>
                                <a href="chinh-sach-ban-hang-uu-dai-times-city-park-hill-thang-122015/index.htm" >
                                    <img width="115" height="68" src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/12/park-8.png"  class="attachment-best-widgetthumb size-best-widgetthumb wp-post-image" alt="Tiến độ thanh toán Tòa Park 8" title="" />                                </a>
                            </div>
                                                        <div class="footer-carousel-item">
                                <div class="dark-style post-box">
                                    <div class="post-data">
                                        <header>
                                            <a href="thi-truong-bds-cuoi-nam-tien-do-ve-dau/index.htm"  class="title post-title" title="Thị trường BĐS cuối năm: Tiền đổ về đâu?">Thị trường BĐS cuối năm: Tiền đổ về đâu?</a>
                                                <div class="post-info">  
                                                    <span class="thetime updated">November 11, 2015</span>    
                                                </div>
                                        </header>
                                    </div>
                                </div>
                                <a href="thi-truong-bds-cuoi-nam-tien-do-ve-dau/index.htm" >
                                    <img width="115" height="58" src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/11/phoi-canh-park-hill-premium.jpg"  class="attachment-best-widgetthumb size-best-widgetthumb wp-post-image" alt="Phối cảnh Park Hill Premium" title="" />                                </a>
                            </div>
                                                        <div class="footer-carousel-item">
                                <div class="dark-style post-box">
                                    <div class="post-data">
                                        <header>
                                            <a href="park-hill-premium-dang-cap-can-ho-uu-viet-chuan-tuong-lai/index.htm"  class="title post-title" title="Park Hill Premium: Đẳng cấp căn hộ ưu việt, chuẩn tương lai">Park Hill Premium: Đẳng cấp căn hộ ưu việt, chuẩn tương lai</a>
                                                <div class="post-info">  
                                                    <span class="thetime updated">November 3, 2015</span>    
                                                </div>
                                        </header>
                                    </div>
                                </div>
                                <a href="park-hill-premium-dang-cap-can-ho-uu-viet-chuan-tuong-lai/index.htm" >
                                    <img width="115" height="65" src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/11/parkhill-timescity2.png"  class="attachment-best-widgetthumb size-best-widgetthumb wp-post-image" alt="Không gian xanh rộng mở tại Park Hill Premium" title="" />                                </a>
                            </div>
                                                        <div class="footer-carousel-item">
                                <div class="dark-style post-box">
                                    <div class="post-data">
                                        <header>
                                            <a href="shophouse-va-smart-home-xu-huong-moi-tai-park-hill-premium/index.htm"  class="title post-title" title="Shophouse và Smart Home – Xu hướng mới tại Park Hill Premium">Shophouse và Smart Home – Xu hướng mới tại Park Hill Premium</a>
                                                <div class="post-info">  
                                                    <span class="thetime updated">October 28, 2015</span>    
                                                </div>
                                        </header>
                                    </div>
                                </div>
                                <a href="shophouse-va-smart-home-xu-huong-moi-tai-park-hill-premium/index.htm" >
                                    <img width="115" height="65" src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/10/be-boi-ngoai-troi-park-hill-premium1.jpg"  class="attachment-best-widgetthumb size-best-widgetthumb wp-post-image" alt="Bể bơi ngoài trời Park Hill Premium" title="" />                                </a>
                            </div>
                                                        <div class="footer-carousel-item">
                                <div class="dark-style post-box">
                                    <div class="post-data">
                                        <header>
                                            <a href="can-ho-thoi-thuong-park-hill-premium-co-hoi-cuoi-cung-so-huu-can-ho-tai-vinhomes-times-city/index.htm"  class="title post-title" title="Căn hộ thời thượng Park Hill Premium &#8211; Cơ hội cuối cùng sở hữu căn hộ tại Vinhomes Times City">Căn hộ thời thượng Park Hill Premium &#8211; Cơ hội cuối cùng sở hữu căn hộ tại Vinhomes Times City</a>
                                                <div class="post-info">  
                                                    <span class="thetime updated">October 26, 2015</span>    
                                                </div>
                                        </header>
                                    </div>
                                </div>
                                <a href="can-ho-thoi-thuong-park-hill-premium-co-hoi-cuoi-cung-so-huu-can-ho-tai-vinhomes-times-city/index.htm" >
                                    <img width="115" height="54" src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/10/can-ho-smart-home-park-hill-premium.jpg"  class="attachment-best-widgetthumb size-best-widgetthumb wp-post-image" alt="Căn hộ Smart Homes Park Hill Premium" title="" srcset="http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/10/can-ho-smart-home-park-hill-premium-300x141.jpg 300w, http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/10/can-ho-smart-home-park-hill-premium-1024x482.jpg 1024w, http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/10/can-ho-smart-home-park-hill-premium.jpg 1600w" sizes="(max-width: 115px) 100vw, 115px" />                                </a>
                            </div>
                                                        <div class="footer-carousel-item">
                                <div class="dark-style post-box">
                                    <div class="post-data">
                                        <header>
                                            <a href="park-hill-premium-va-nhung-con-so/index.htm"  class="title post-title" title="Park Hill Premium và những con số">Park Hill Premium và những con số</a>
                                                <div class="post-info">  
                                                    <span class="thetime updated">October 26, 2015</span>    
                                                </div>
                                        </header>
                                    </div>
                                </div>
                                <a href="park-hill-premium-va-nhung-con-so/index.htm" >
                                    <img width="115" height="98" src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/10/Park-hill-premium-tien-ich.jpg"  class="attachment-best-widgetthumb size-best-widgetthumb wp-post-image" alt="Tiện ích dự án Park Hill Premium" title="" srcset="http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/10/Park-hill-premium-tien-ich-300x256.jpg 300w, http://parkhilltimescity.com/wp-content/themes/faci-qsoft/uploads/sites/133/2015/10/Park-hill-premium-tien-ich.jpg 1000w" sizes="(max-width: 115px) 100vw, 115px" />                                </a>
                            </div>
                                                        <div class="footer-carousel-item">
                                <div class="dark-style post-box">
                                    <div class="post-data">
                                        <header>
                                            <a href="toa-park-10-park-hill-premium/index.htm"  class="title post-title" title="Park 10 Park Hill Premium &#8211; Chung cư Vàng tại Hà Nội">Park 10 Park Hill Premium &#8211; Chung cư Vàng tại Hà Nội</a>
                                                <div class="post-info">  
                                                    <span class="thetime updated">October 24, 2015</span>    
                                                </div>
                                        </header>
                                    </div>
                                </div>
                                <a href="toa-park-10-park-hill-premium/index.htm" >
                                    <img width="115" height="71" src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/10/thiet-ke-can-park-10.jpg"  class="attachment-best-widgetthumb size-best-widgetthumb wp-post-image" alt="Căn hộ Park 10 với ưu thế vượt trội từ chủ đầu tư" title="" />                                </a>
                            </div>
                                                        <div class="footer-carousel-item">
                                <div class="dark-style post-box">
                                    <div class="post-data">
                                        <header>
                                            <a href="park-11-park-hill-times-city/index.htm"  class="title post-title" title="Ra mắt Park 11 &#8211; Park Hill Times City">Ra mắt Park 11 &#8211; Park Hill Times City</a>
                                                <div class="post-info">  
                                                    <span class="thetime updated">October 24, 2015</span>    
                                                </div>
                                        </header>
                                    </div>
                                </div>
                                <a href="park-11-park-hill-times-city/index.htm" >
                                    <img width="115" height="77" src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/10/Park-11-park-hill-premium.jpg"  class="attachment-best-widgetthumb size-best-widgetthumb wp-post-image" alt="Park 11 Park Hill Premium" title="" />                                </a>
                            </div>
                                                        <div class="footer-carousel-item">
                                <div class="dark-style post-box">
                                    <div class="post-data">
                                        <header>
                                            <a href="toa-park-9-park-hill-premium/index.htm"  class="title post-title" title="Tòa Park 9 &#8211; Park Hill Premium với thiết kế vượt trội">Tòa Park 9 &#8211; Park Hill Premium với thiết kế vượt trội</a>
                                                <div class="post-info">  
                                                    <span class="thetime updated">October 24, 2015</span>    
                                                </div>
                                        </header>
                                    </div>
                                </div>
                                <a href="toa-park-9-park-hill-premium/index.htm" >
                                    <img width="115" height="76" src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/10/toa-Park-9-park-hill-premium.jpg"  class="attachment-best-widgetthumb size-best-widgetthumb wp-post-image" alt="Park 9 Park Hill Premium" title="" />                                </a>
                            </div>
                                                    </div>
                    </div>
                </div><!-- slider-container -->
            </div>
        </div>
                
            <div class="container">
                            <div class="footer-widgets top-footer-widgets widgets-num-3">
                                    <div class="f-widget f-widget-1">
                        <div id="text-11" class="widget widget_text"><h3 class="widget-title">BẢN ĐỒ DỰ ÁN</h3>			<div class="textwidget">
<div class="googlemaps left"><iframe width="350" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="../maps.google.com/maps-q=Khu+đô+thị+Times+City,+Vĩnh+Tuy,+Hai+Bà+Trưng,+Hà+Nội,+Vietnam&output=embed" ></iframe></div></div>
		</div>                    </div>
                                        <div class="f-widget f-widget-2">
                        <div id="text-9" class="widget widget_text"><h3 class="widget-title">ĐĂNG KÝ NHẬN THÔNG TIN</h3>			<div class="textwidget"><!-- This site converts visitors into subscribers and customers with the OptinMonster WordPress plugin v2.1.7 - http://optinmonster.com/ -->
<div id="om-jze8jwpmdh-post" class="optin-monster-overlay" style=""><script type="text/javascript" src="../ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" ></script><style type="text/css" class="om-theme-sample-styles">.optin-monster-success-message {font-size: 21px;font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;color: #282828;font-weight: 300;text-align: center;margin: 0 auto;}.optin-monster-success-overlay .om-success-close {font-size: 32px !important;font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif !important;color: #282828 !important;font-weight: 300 !important;position: absolute !important;top: 0px !important;right: 10px !important;background: none !important;text-decoration: none !important;width: auto !important;height: auto !important;display: block !important;line-height: 32px !important;padding: 0 !important;}.om-helper-field {display: none !important;visibility: hidden !important;opacity: 0 !important;height: 0 !important;line-height: 0 !important;}html div#om-jze8jwpmdh-post * {box-sizing:border-box;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;}html div#om-jze8jwpmdh-post {background:none;border:0;border-radius:0;-webkit-border-radius:0;-moz-border-radius:0;float:none;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;height:auto;letter-spacing:normal;outline:none;position:static;text-decoration:none;text-indent:0;text-shadow:none;text-transform:none;width:auto;visibility:visible;overflow:visible;margin:0;padding:0;line-height:1;box-sizing:border-box;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-shadow:none;-moz-box-shadow:none;-ms-box-shadow:none;-o-box-shadow:none;box-shadow:none;-webkit-appearance:none;}html div#om-jze8jwpmdh-post .om-clearfix {clear: both;}html div#om-jze8jwpmdh-post .om-clearfix:after {clear: both;content: ".";display: block;height: 0;line-height: 0;overflow: auto;visibility: hidden;zoom: 1;}html div#om-jze8jwpmdh-post #om-post-sample-optin {background: #fff;position: relative;padding: 20px;text-align: center;margin: 0 auto;max-width: 100%;width: 100%;}html div#om-jze8jwpmdh-post #om-post-sample-optin-title {font-size: 18px;color: #222;width: 100%;margin-bottom: 15px;}html div#om-jze8jwpmdh-post #om-post-sample-optin-tagline {font-size: 16px;line-height: 1.25;color: #484848;width: 100%;margin-bottom: 15px;}html div#om-jze8jwpmdh-post input,html div#om-jze8jwpmdh-post #om-post-sample-optin-name,html div#om-jze8jwpmdh-post #om-post-sample-optin-email {background-color: #fff;width: 100%;border: 1px solid #ddd;font-size: 16px;line-height: 24px;padding: 4px 6px;overflow: hidden;outline: none;margin: 0 0 10px;vertical-align: middle;display: inline;color: #222;height: 34px;}html div#om-jze8jwpmdh-post input[type=submit],html div#om-jze8jwpmdh-post button,html div#om-jze8jwpmdh-post #om-post-sample-optin-submit {background: #ff370f;border: 1px solid #ff370f;color: #fff;font-size: 16px;padding: 4px 6px;line-height: 24px;text-align: center;vertical-align: middle;cursor: pointer;display: inline;margin: 0;width: 100%;}html div#om-jze8jwpmdh-post input[type=checkbox],html div#om-jze8jwpmdh-post input[type=radio] {-webkit-appearance: checkbox;width: auto;outline: invert none medium;padding: 0;margin: 0;}</style><div id="om-post-sample-optin" class="om-post-sample om-clearfix om-theme-sample om-custom-html-form" style="background-color:#ffffff"><div id="om-post-sample-optin-wrap" class="om-clearfix"><div id="om-post-sample-header" class="om-clearfix" data-om-action="selectable"><div id="om-post-sample-optin-title" data-om-action="editable" data-om-field="title" style="color:#222222;font-family:Helvetica;font-size:18px;"></div></div><div id="om-post-sample-content" class="om-clearfix" data-om-action="selectable"><div id="om-post-sample-optin-tagline" data-om-action="editable" data-om-field="tagline" style="color:#484848;font-family:Helvetica;font-size:16px;"></div></div><div id="om-post-sample-footer" class="om-clearfix om-has-email" data-om-action="selectable"><form name="Vinhomes Parkhill" action="http://crm.ancu.com/modules/Webforms/capture.php" method="post" accept-charset="utf-8" enctype="multipart/form-data">
   <input type="hidden" name="__vtrftk" value="sid:dd1da5e57c1aa3afa3c8175cfe670d604076147b,1457338947">
   <input type="hidden" name="publicid" value="d1e29e9c487e85f090f3b197d2ba3b7a">
   <input type="hidden" name="name" value="Vinhomes Parkhill">
   <input type="hidden" name="VTIGER_RECAPTCHA_PUBLIC_KEY" value="RECAPTCHA PUBLIC KEY FOR THIS DOMAIN">
   <table style="width:100%;border:none;">
      <tbody>
         <tr>
            <td>
               <input type="text" placeholder="Họ và Tên*" name="lastname" value="" required="">                
            </td>
         </tr>
         <tr>
            <td>
               <input type="text" placeholder="Số điện thoại*" name="mobile" value="" required="">                
            </td>
         </tr>
         <tr>
            <td>
               <input type="email" placeholder="Email" name="email" value="">                
            </td>
         </tr>
         <tr style="display:none">
            <td><input name="changeDesc" value="Park Hill - Nhu cầu Park Hill Premium" hidden=""></td>
         </tr>
         <tr style="display:none">
            <td>
               <select name="leadsource" hidden="">
                  <option value="">Select Value</option>
                  <option value="Website" selected="">Website</option>
               </select>
            </td>
         </tr>
         <tr style="display:none">
            <td>
               <input type="hidden" placeholder="Campaign" name="label:Campaign" value="">                
            </td>
         </tr>
      </tbody>
   </table>
   <input type="submit" value="ĐĂNG KÝ">
</form></div></div><input type="email" name="email" value="" class="om-helper-field" /><input type="text" name="website" value="" class="om-helper-field" /></div><script type="text/javascript">jQuery(document).ready(function($){});</script></div>
<script type="text/javascript">var jze8jwpmdh_post, omo = {"id":335,"optin":"jze8jwpmdh-post","campaign":"Form Footer","clones":[""],"hash":"jze8jwpmdh-post","optin_js":"jze8jwpmdh_post","type":"post","theme":"sample","cookie":0,"delay":0,"second":false,"exit":false,"redirect":"http:\/\/parkhilltimescity.com\/thank-you\/","redirect_pass":false,"custom":true,"test":false,"global_cookie":false,"preview":false,"ajax":"http:\/\/parkhilltimescity.com\/?optin-monster-ajax-route=1","mobile":false,"post_id":727,"preloader":"http:\/\/parkhilltimescity.com\/wp-content\/plugins\/optin-monster\/assets\/css\/images\/preloader.gif","error":"There was an error with your submission. Please try again.","ajax_error":"There was an error with the AJAX request: ","name_error":"Please enter a valid name.","email_error":"Please enter a valid email address.","bot_error":"Honeypot fields have been activated. Your submission is being flagged as potential spam.","success":"Thanks for subscribing! Please check your email for further instructions."}; jze8jwpmdh_post = new OptinMonster(); jze8jwpmdh_post.init(omo);</script>
<!-- / OptinMonster WordPress plugin. -->
</div>
		</div>                    </div>
                                        <div class="f-widget f-widget-3">
                        <div id="text-12" class="widget widget_text"><h3 class="widget-title">VỀ CHÚNG TÔI</h3>			<div class="textwidget"><p style="text-align: center"><b><a href="wp-content/themes/faci-qsoft/uploads/sites/133/2015/11/logo_vietstar.gif" ><img class="aligncenter size-full wp-image-1257" src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/11/logo_vietstar.gif"  alt="logo_vietstar" width="294" height="73" /></a>CÔNG TY CỔ PHẦN KINH DOANH DV TỔNG HỢP VIETSTARLAND</b></p>
<p style="text-align: center">L2-T6 Vinhomes Times City L2-R3 Vinhomes Royal City Lô 12 - Hoa Sữa 3 - Vinhomes Riverside</p>
<p style="text-align: center"><strong>Hotline: 0965 85 56 85</strong></p></div>
		</div>                    </div>
                                    </div><!--.top-footer-widgets-->
                        </div>
            
        <div class="copyrights">
            <div class="container">
                <!--start copyrights-->
<div id="copyright-note">
<span><a href="index.htm"  title="Park Hill Times City" rel="nofollow"></a> Copyright &copy; 2016.</span>
<div class="right"></div>
</div>
<!--end copyrights-->
            </div><!--.container-->
        </div><!--.copyrights-->
    </footer><!--footer-->
</div><!--.main-container-wrap-->
        <!--start footer code-->
        <!-- Google Tag Manager -->
<noscript><iframe src="../www.googletagmanager.com/ns.html-id=GTM-MB8RWC" 
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MB8RWC');</script>
<!-- End Google Tag Manager -->
    <!--end footer code-->
    <script language="javascript">function vs_setCookie(cname, cvalue, exdays) {var d = new Date();d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));var expires = "expires=" + d.toUTCString(); document.cookie = cname + "= " + "; expires=Thu, 01 Jan 1970 00:00:00 UTC"; document.cookie = cname + "=" + cvalue + "; " + expires+ "; path=/";}function vs_getCookie(cname) {var name = cname + "=";var ca = document.cookie.split(";"); for (var i = 0; i < ca.length; i++) {var c = ca[i];while (c.charAt(0) == " ") c = c.substring(1);if (c.indexOf(name) != -1) return c.substring(name.length, c.length);}return "";}function vs_checkCookie(cname) {var user = vs_getCookie(cname); var strValue = "R= "; user = user.toLowerCase(); if (user == "" || (strValue.indexOf("utm_source") != -1 && user.indexOf("utm_source") == -1) || (strValue.indexOf("utm_medium") != -1 && user.indexOf("utm_medium") == -1) || (strValue.indexOf("utm_campaign") != -1 && user.indexOf("utm_campaign") == -1) ) {vs_setCookie(cname, strValue, 365);}}vs_checkCookie("CRM_COOKIES_PARKHILLTIMESCITY_COM");</script><script type='text/javascript'>
/* <![CDATA[ */
var wpmm = {"ajaxurl":"http:\/\/parkhilltimescity.com\/wp-admin\/admin-ajax.php","container_selector":".primary-navigation","css_class":"wpmm"};
/* ]]> */
</script>
<script type='text/javascript' src="<?php echo get_stylesheet_directory_uri(); ?>/plugins/wp-mega-menu/js/wpmm.js" ></script>
<script type='text/javascript' src="wp-content/themes/faci-qsoft/js/owl.carousel.min.js" ></script>
<script type='text/javascript' src="wp-includes/js/wp-embed.min.js" ></script>
<script type='text/javascript' src="wp-content/themes/faci-qsoft/js/jquery.magnific-popup.min.js" ></script>
<script type='text/javascript' async="async" src="wp-content/themes/faci-qsoft/js/sticky.js" ></script>
<!-- This site converts visitors into subscribers and customers with the OptinMonster WordPress plugin v2.1.7 - http://optinmonster.com/ -->
<div id="om-xpkji90lro-lightbox" class="optin-monster-overlay" style="display:none;"><script type="text/javascript" src="../ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" ></script><style type="text/css" class="om-theme-case-study-styles">.optin-monster-success-message {font-size: 21px;font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;color: #282828;font-weight: 300;text-align: center;margin: 0 auto;}.optin-monster-success-overlay .om-success-close {font-size: 32px !important;font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif !important;color: #282828 !important;font-weight: 300 !important;position: absolute !important;top: 0px !important;right: 10px !important;background: none !important;text-decoration: none !important;width: auto !important;height: auto !important;display: block !important;line-height: 32px !important;padding: 0 !important;}.om-helper-field {display: none !important;visibility: hidden !important;opacity: 0 !important;height: 0 !important;line-height: 0 !important;}html div#om-xpkji90lro-lightbox * {box-sizing:border-box;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;}html div#om-xpkji90lro-lightbox {background:none;border:0;border-radius:0;-webkit-border-radius:0;-moz-border-radius:0;float:none;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing: grayscale;height:auto;letter-spacing:normal;outline:none;position:static;text-decoration:none;text-indent:0;text-shadow:none;text-transform:none;width:auto;visibility:visible;overflow:visible;margin:0;padding:0;line-height:1;box-sizing:border-box;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-shadow:none;-moz-box-shadow:none;-ms-box-shadow:none;-o-box-shadow:none;box-shadow:none;-webkit-appearance:none;}html div#om-xpkji90lro-lightbox {background: rgb(0, 0, 0);background: rgba(0, 0, 0, .7);font-family: helvetica,arial,sans-serif;-moz-osx-font-smoothing: grayscale;-webkit-font-smoothing: antialiased;line-height: 1;width: 100%;height: 100%;}html div#om-xpkji90lro-lightbox .om-clearfix {clear: both;}html div#om-xpkji90lro-lightbox .om-clearfix:after {clear: both;content: ".";display: block;height: 0;line-height: 0;overflow: auto;visibility: hidden;zoom: 1;}html div#om-xpkji90lro-lightbox #om-lightbox-case-study-optin {background: #fff;display: none;position: absolute;top: 50%;left: 50%;min-height: 175px;max-width: 700px;width: 100%;z-index: 734626274;}html div#om-xpkji90lro-lightbox #om-lightbox-case-study-optin-wrap {position: relative;height: 100%;}html div#om-xpkji90lro-lightbox .om-close {position: absolute;top: -12px;right: -12px;text-decoration: none !important;display: block;width: 35px;height: 35px;background: url("<?php echo get_stylesheet_directory_uri(); ?>/plugins/optin-monster/assets/css/images/close.png") no-repeat scroll 0 0;z-index: 1500;}html div#om-xpkji90lro-lightbox #om-lightbox-case-study-optin-bar {margin-bottom: 20px;clear: both;}html div#om-xpkji90lro-lightbox .om-bar {float: left;width: 25%;height: 11px;display: block;}html div#om-xpkji90lro-lightbox .om-red-bar {background-color: #ef3e36;}html div#om-xpkji90lro-lightbox .om-green-bar {background-color: #abb92e;}html div#om-xpkji90lro-lightbox .om-orange-bar {background-color: #f57826;}html div#om-xpkji90lro-lightbox .om-blue-bar {background-color: #17b4e9;}html div#om-xpkji90lro-lightbox #om-lightbox-case-study-header {min-height: 30px;padding: 15px;width: 100%;}html div#om-xpkji90lro-lightbox #om-lightbox-case-study-optin-title {font-size: 24px;color: #484848;width: 100%;margin-bottom: 15px;text-shadow: 0 1px #fff;}html div#om-xpkji90lro-lightbox #om-lightbox-case-study-optin-tagline {font-size: 16px;line-height: 1.25;color: #484848;width: 100%;margin-bottom: 30px;text-shadow: 0 1px #fff;}html div#om-xpkji90lro-lightbox #om-lightbox-case-study-content {padding: 15px;}html div#om-xpkji90lro-lightbox #om-lightbox-case-study-left {float: left;max-width: 280px;width: 100%;position: relative;}html div#om-xpkji90lro-lightbox #om-lightbox-case-study-optin-image-container {position: relative;max-width: 280px;max-height: 245px;margin: 0 auto;}html div#om-xpkji90lro-lightbox #om-lightbox-case-study-optin-image-container img {display: block;margin: 0 auto;text-align: center;height: auto;max-width: 100%;}html div#om-xpkji90lro-lightbox #om-lightbox-case-study-right {float: right;max-width: 370px;width: 100%;background-color: #dadada;margin-right: -15px;padding: 20px;position: relative;}html div#om-xpkji90lro-lightbox #om-lightbox-case-study-arrow {position: absolute;width: 49px;height: 56px;left: -40px;top: -40px;}html div#om-xpkji90lro-lightbox label {color: #333;}html div#om-xpkji90lro-lightbox input,html div#om-xpkji90lro-lightbox #om-lightbox-case-study-optin-name,html div#om-xpkji90lro-lightbox #om-lightbox-case-study-optin-email {background-color: #fff;width: 100%;border: 1px solid #ccc;font-size: 16px;line-height: 24px;padding: 10px 6px;overflow: hidden;outline: none;margin: 0 0 15px;vertical-align: middle;height: 46px;}html div#om-xpkji90lro-lightbox input[type=submit],html div#om-xpkji90lro-lightbox button,html div#om-xpkji90lro-lightbox #om-lightbox-case-study-optin-submit {background: #484848;border: 1px solid #484848;width: 100%;color: #fff;font-size: 20px;padding: 12px 6px;line-height: 28px;text-align: center;vertical-align: middle;cursor: pointer;margin: 0;height: 54px;}html div#om-xpkji90lro-lightbox input[type=checkbox],html div#om-xpkji90lro-lightbox input[type=radio] {-webkit-appearance: checkbox;width: auto;outline: invert none medium;padding: 0;margin: 0;}@media (max-width: 700px) {html div#om-xpkji90lro-lightbox #om-lightbox-case-study-optin[style] {width: 100%;position: relative;}html div#om-xpkji90lro-lightbox .om-close {right: 9px;}html div#om-xpkji90lro-lightbox #om-lightbox-case-study-left,html div#om-xpkji90lro-lightbox #om-lightbox-case-study-right,html div#om-xpkji90lro-lightbox #om-lightbox-case-study-optin-name,html div#om-xpkji90lro-lightbox #om-lightbox-case-study-optin-email,html div#om-xpkji90lro-lightbox #om-lightbox-case-study-optin-submit,html div#om-xpkji90lro-lightbox .om-has-email #om-lightbox-case-study-optin-email {float: none;width: 100%;max-width: 100%;}html div#om-xpkji90lro-lightbox #om-lightbox-case-study-left,html div#om-xpkji90lro-lightbox #om-lightbox-case-study-optin-name,html div#om-xpkji90lro-lightbox #om-lightbox-case-study-optin-email {margin-bottom: 15px;}}@media only screen and (-webkit-min-device-pixel-ratio: 2),only screen and (min--moz-device-pixel-ratio: 2),only screen and (-o-min-device-pixel-ratio: 2/1),only screen and (min-device-pixel-ratio: 2),only screen and (min-resolution: 192dpi),only screen and (min-resolution: 2dppx) {html div#om-xpkji90lro-lightbox .om-close {background-image: url("<?php echo get_stylesheet_directory_uri(); ?>/plugins/optin-monster/assets/css/images/close@2x.png");background-size: 35px 35px;}}</style><div id="om-lightbox-case-study-optin" class="om-lightbox-case-study om-clearfix om-theme-case-study om-custom-html-form"><div id="om-lightbox-case-study-optin-wrap" class="om-clearfix"><a href="#" class="om-close" title="Close"></a><div id="om-lightbox-case-study-optin-bar" class="om-clearfix"><span class="om-bar om-red-bar"></span><span class="om-bar om-green-bar"></span><span class="om-bar om-orange-bar"></span><span class="om-bar om-blue-bar"></span></div><div id="om-lightbox-case-study-header" class="om-clearfix"><div id="om-lightbox-case-study-optin-title" data-om-action="editable" data-om-field="title" style="color:#484848;font-family:Bree Serif;font-size:32px;text-align:center;"><span style="font-weight:bold;"><span style="color:#FF0000;"><span style="font-family:roboto;"><span style="font-size:36px;"><span style="font-size:48px;">HOT!!!</span></span></span></span><span style="color:#FF8C00;"><span style="font-family:roboto;"><span style="font-size:36px;"> RA MẮT PARK HILL PREMIUM</span></span></span></span></div><div id="om-lightbox-case-study-optin-tagline" data-om-action="editable" data-om-field="tagline" style="color:#484848;font-family:Open Sans;font-size:24px;text-align:center;"><span style="font-weight:bold;"><span style="font-family:roboto;"><span style="font-size:28px;"><span style="color:#008000;">SẢN PHẨM MỚI, ĐẲNG CẤP HƠN, GI&Aacute; KH&Ocirc;NG ĐỔI</span></span></span><br />
<span style="color:#006400;">CHIẾT KHẤU KHỦNG</span><span style="color:#FF0000;"> TỚI 7%/NĂM</span><br />
<span style="font-size:20px;">HOTLINE: 0965 85 56 85</span></span></div></div><div id="om-lightbox-case-study-content" class="om-clearfix"><div id="om-lightbox-case-study-left"><div id="om-lightbox-case-study-optin-image-container" class="om-image-container om-clearfix"><img class="optin-monster-image optin-monster-image-lightbox-case-study" src="wp-content/themes/faci-qsoft/uploads/sites/133/2015/03/Park-hill-times-city-phoi-canh-ngoai-troi-3-280x245.jpg"  alt="Park-hill-times-city-phoi-canh-ngoai-troi-3" title="Park-hill-times-city-phoi-canh-ngoai-troi-3" /></div></div><div id="om-lightbox-case-study-right"><img id="om-lightbox-case-study-arrow" src="<?php echo get_stylesheet_directory_uri(); ?>/plugins/optin-monster/includes/themes/case-study/images/arrow.png"  alt="Arrow" /><form name="Vinhomes Parkhill" action="http://crm.ancu.com/modules/Webforms/capture.php" method="post" accept-charset="utf-8" enctype="multipart/form-data">
   <input type="hidden" name="__vtrftk" value="sid:dd1da5e57c1aa3afa3c8175cfe670d604076147b,1457338947">
   <input type="hidden" name="publicid" value="d1e29e9c487e85f090f3b197d2ba3b7a">
   <input type="hidden" name="name" value="Vinhomes Parkhill">
   <input type="hidden" name="VTIGER_RECAPTCHA_PUBLIC_KEY" value="RECAPTCHA PUBLIC KEY FOR THIS DOMAIN">
   <table style="width:100%;border:none;">
      <tbody>
         <tr>
            <td>
               <input type="text" placeholder="Họ và Tên*" name="lastname" value="" required="">                
            </td>
         </tr>
         <tr>
            <td>
               <input type="text" placeholder="Số điện thoại*" name="mobile" value="" required="">                
            </td>
         </tr>
         <tr>
            <td>
               <input type="email" placeholder="Email" name="email" value="">                
            </td>
         </tr>
         <tr style="display:none">
            <td><input name="changeDesc" value="Park Hill - (popup) Nhu cầu Park Hill Premium" hidden=""></td>
         </tr>
         <tr style="display:none">
            <td>
               <select name="leadsource" hidden="">
                  <option value="">Select Value</option>
                  <option value="Website" selected="">Website</option>
               </select>
            </td>
         </tr>
         <tr style="display:none">
            <td>
               <input type="hidden" placeholder="Campaign" name="label:Campaign" value="">                
            </td>
         </tr>
      </tbody>
   </table>
   <input type="submit" value="ĐĂNG KÝ NGAY">
</form></div></div></div><input type="email" name="email" value="" class="om-helper-field" /><input type="text" name="website" value="" class="om-helper-field" /></div><script type="text/javascript">jQuery(document).ready(function($){WebFont.load({google: {families: ['Open+Sans%7CBree+Serif%7CRoboto']}});});</script></div>
<script type="text/javascript">var xpkji90lro_lightbox, omo = {"id":644,"optin":"xpkji90lro-lightbox","campaign":"pop up","clones":[""],"hash":"xpkji90lro-lightbox","optin_js":"xpkji90lro_lightbox","type":"lightbox","theme":"case-study","cookie":30,"delay":20000,"second":0,"exit":0,"redirect":"http:\/\/parkhilltimescity.com\/thank-you\/","redirect_pass":false,"custom":true,"test":false,"global_cookie":0,"preview":false,"ajax":"http:\/\/parkhilltimescity.com\/?optin-monster-ajax-route=1","mobile":false,"post_id":727,"preloader":"http:\/\/parkhilltimescity.com\/wp-content\/plugins\/optin-monster\/assets\/css\/images\/preloader.gif","error":"There was an error with your submission. Please try again.","ajax_error":"There was an error with the AJAX request: ","name_error":"Please enter a valid name.","email_error":"Please enter a valid email address.","bot_error":"Honeypot fields have been activated. Your submission is being flagged as potential spam.","success":"Thanks for subscribing! Please check your email for further instructions.","exit_sensitivity":20}; xpkji90lro_lightbox = new OptinMonster(); xpkji90lro_lightbox.init(omo);</script>
<!-- / OptinMonster WordPress plugin. -->
</body>
</html>