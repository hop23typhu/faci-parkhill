<?php get_header(); ?>

<!-- ==================start content body section=============== -->
<section id="contentbody">
  <div class="container">
    <div class="row">
    <!-- start left bar content -->
      <div class=" col-sm-12 col-md-6 col-lg-6">
     
        <div class="row">
          <div class="leftbar_content">
            <h2>Bài viêt gần đây</h2>
            <section id="post_content" role="main">
              <?php 
              $args = array( 'post_type' => 'post', 'posts_per_page' => 2);
              $wp_query = new WP_Query( $args );
                if($wp_query->have_posts()):
                  while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
                    get_template_part('template-parts/list','home');
                  endwhile;
                wp_reset_postdata();
                else :  get_template_part('template-parts/content','home');
                endif;
              ?>
          </section>
          <p class="post-loadmore-info"><a id="post_loadmore" href="#" >Xem nhiều hơn</a></p>
          <script type="text/javascript">
            jQuery(document).ready(function(){  
                var page = 1;    
                jQuery("#post_loadmore").click(function() {   
                    jQuery.ajax({
                        type: "POST",
                        url: "<?php echo admin_url('admin-ajax.php'); ?>",
                        data: {
                                  action: "event_list",
                                  page: page
                              },
                        success: function(data) {                                    
                          page++;
                          $("#post_content").append(data);
                          if(data.indexOf('end') > 0) {
                            $('.post-loadmore-info').hide();
                          }
                        }
                    }); 
                    return false;
                });
            });
          </script>







           <!--  <?php 
             if(have_posts()):while(have_posts()):the_post();
           ?>
           start single stuff post
           <div class="single_stuff wow fadeInDown">
             <div class="single_stuff_img">
               <a href="<?php the_permalink(); ?>">
               <?php 
                 if(has_post_thumbnail()) the_post_thumbnail('blog-thumb',array('class'=>'img-responsive','alt'=>get_the_title()));
                 else echo '<img src=""/>';
               ?>
               </a>
             </div>
             <div class="single_stuff_article">
                 <div class="single_sarticle_inner">
                     <?php the_category(' '); ?>
                   <div class="stuff_article_inner">
                     <span class="stuff_date"><?php the_date('d-m'); ?> <strong></strong></span>
                     <h2>
                       <a href="<?php the_permalink(); ?>">
                         <?php the_title(); ?>
                       </a>
                     </h2>
                     <p><?php the_excerpt(); ?></p>
                   </div>
                 </div>
             </div>
           </div>
           End single stuff post
                 <?php 
                   endwhile;
                   else :get_template_part('template-parts','none'); 
                   endif;
                 ?>   -->
          

           
          </div>
        </div>  
      </div>
      <!-- End left bar content -->
	  <?php //get_sidebar(); ?>
      
    </div>
  </div>
</section>
<?php 
	$q_sort=new WP_Query(array('orderby'=>'menu_order','order'=>'asc'));

	while($q_sort->have_posts()):$q_sort->the_post();
		echo get_the_title( ).'<br>';
	endwhile;
?>
<?php get_footer(); ?>