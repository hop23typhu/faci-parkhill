jQuery(function($) {
	var map, marker;
	$('.map-canvas').each(function(){
		var elem = $(this);
		var address = elem.data('address');
		var lat = elem.data('lat');
		var lng = elem.data('lng');
		var zoom = elem.data('zoom');
		var latlng = new google.maps.LatLng(lat, lng);

		var mapOptions = {
            zoom: zoom,
            center: latlng,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
		    mapTypeControl: true,
		    mapTypeControlOptions: {
		      	style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
		    },
		    zoomControl: true,
		    zoomControlOptions: {
		      	style: google.maps.ZoomControlStyle.SMALL
		    },
		    mapTypeId: google.maps.MapTypeId.ROADMAP
        };
		map    = new google.maps.Map(elem.context, mapOptions);
		marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: address
        });
        
        var infowindow = new google.maps.InfoWindow({
		  content: address
		});
        
        infowindow.open(map,marker);

		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open(map,marker);
		});
	})
});