<?php 
//get post
/*
	<section id="content" role="main">
			<?php 
			$args = array( 'post_type' => 'post', 'cat' => 4, 'posts_per_page' => 6);
			$wp_query = new WP_Query( $args );
			while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?> 
			<div class="page-project-info col-md-4 col-sm-6 col-xs-12">
				<div class="project-thumb">
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('category-thumb'); ?></a>
				</div>
				<h5><a href="<?php the_permalink(); ?>"><?php echo short_title('', 12); ?></a></h5> 
			</div>
		<?php endwhile;?>
	</section>
	<p class="project-loadmore-info"><a id="project-loadmore" href="#">Xem nhiều hơn</a></p>
*/

//khai bao header
	<script type="text/javascript">my_base_url = '<?php echo home_url() ?>';</script>
//ajax load more -jquery
	var page = 1;
	$("#project-loadmore").click(function(e) {
		e.preventDefault();
		$.ajax({
			type: 'POST',
			url: my_base_url + '/wp-admin/admin-ajax.php',
			data: {
				action: 'event_list',
				page: page
			},
			success: function(data){
				page++;
				$("#content").append(data);
				if(data.indexOf('end') > 0) {
					$('.project-loadmore-info').hide();
				}
			}
		});
	});
//function
	add_action('wp_ajax_event_list', 'my_load_event_list');
	add_action('wp_ajax_nopriv_event_list', 'my_load_event_list');

	function my_load_event_list(){
		$page = $_POST['page'];
		$df_offset = 6;
		$offset = $page*$df_offset;

		$all_posts = query_posts(array('post_type' =>'post' , 'cat' => 4));
		$count_posts = count($all_posts);

		query_posts(array('post_type' =>'post' , 'cat' => 4, 'posts_per_page'=>$df_offset, 'offset'=> $offset, 'orderby' => 'date' ));

		if (have_posts()):while (have_posts()):the_post();

		if($count_posts <= $df_offset + ($page*$df_offset)) { ?>
		<div class="page-project-info col-md-4 col-sm-6 col-xs-12">
			<div class="project-thumb">
				<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('category-thumb'); ?></a>
			</div>
			<h5><a href="<?php the_permalink(); ?>"><?php echo short_title('', 12); ?></a></h5> 
		</div>
		<!-- end -->
		<?php } else { ?>
		<div class="page-project-info col-md-4 col-sm-6 col-xs-12">
			<div class="project-thumb">
				<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('category-thumb'); ?></a>
			</div>
			<h5><a href="<?php the_permalink(); ?>"><?php echo short_title('', 12); ?></a></h5> 
		</div>
		<!-- continue -->
		<?php }
		endwhile; endif; die(); 
	}




